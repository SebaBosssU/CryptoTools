define('two/crypter/ui', ['two/crypter', 'two/locale', 'two/ui', 'two/ui/autoComplete', 'two/FrontButton', 'two/utils', 'two/eventQueue', 'ejs', 'struct/MapData', 'cdn'], function(CryptoTools, Locale, Interface, autoComplete, FrontButton, utils, eventQueue, ejs, $mapData, cdn) {
    var ui
    var opener
    var $window
    var bindEvents = function() {
    }
    var CrypterInterface = function() {
        ui = new Interface('CryptoTools', {
            activeTab: 'crypter',
            template: '__crypter_html_window',
            css: '__crypter_css_style',
            replaces: {
                locale: Locale,
                version: CryptoTools.version,
                types: ['village', 'character']
            }
        })
        opener = new FrontButton(Locale('crypter', 'title'), {
            classHover: false,
            classBlur: false,
            onClick: function() {
                ui.openWindow()
            }
        })
        $window = $(ui.$window)
        bindEvents()
        CryptoTools.interfaceInitialized = true
        return ui
    }
    CryptoTools.interface = function() {
        CryptoTools.interface = CrypterInterface()
    }
})