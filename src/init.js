require([
    'two/ready',
    'two/crypter',
    'two/crypter/ui'
], function (
    ready,
    CryptoTools
) {
    if (CryptoTools.initialized) {
        return false
    }

    ready(function () {
        CryptoTools.init()
        CryptoTools.interface()
        CryptoTools.run()
    })
})
