define('two/crypter', [
    'two/locale',
    'two/ready'
], function (
    Locale,
    ready
) {
    var CryptoTools = {}
    CryptoTools.version = '__crypter_version'
    CryptoTools.init = function () {
        Locale.create('crypter', __crypter_locale, 'pl')

        CryptoTools.initialized = true
    }
    CryptoTools.run = function () {
        if (!CryptoTools.interfaceInitialized) {
            throw new Error('CryptoTools interface not initialized')
        }
        ready(function () {
            $player = modelDataService.getSelectedCharacter()
        }, ['initial_village'])
    }
    return CryptoTools
})